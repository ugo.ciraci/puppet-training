class greetings (
  $message = 'World'
) {
    notify { "Hello ${message}":
    }
}

class { 'greetings':
    message => "Ugo"
}
