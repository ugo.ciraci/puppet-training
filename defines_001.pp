define text_file {
 file {"${name}.txt":
   ensure => 'present',

 }
}

text_file {"/tmp/one":
}

text_file {"/tmp/two":
}
