class greetings (
  $message = 'World'
) {
    $admin = $message ? {
       'Ugo' => true,
       default => false
    }

    notify { "Hello ${message} [$admin]":
    }

    case $admin {
       true: {notify{"Admin user":}}
       false: {notify{"Not admin user":}}
    }

    if $message in ['Ugo', 'Sabrina'] {
       notify{"How are you?":}
    }
    elsif $message != 'Luca' {
       notify{"I am lucky, I don't speak barese!":}
    }
    elsif $message == 'Blerti' {
       notify{"ç'kemi":}
    }
    else {
       notify{"Access denied!":}
    }
}

class { 'greetings':
    message => "Daniele"
}
